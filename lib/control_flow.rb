# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  lower = ('a'..'z').to_a.join
  str.delete(lower)
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  str.length.odd? ? str[str.length/2] : str[(str.length/2)-1, 2]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  cons = ('a'..'z').to_a.join.delete("aeiou")
  str.delete(cons).length

  #This works also but I think it does more work than delete?
  #str.each_char.count {|l| (/[aeiou]/ =~ l) != nil}
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  res = ""
  for i in (0...arr.length)
    res += arr[i]
    res += separator unless i == arr.length - 1
  end
  res
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  for i in (0...str.length)
    i.odd? ? str[i] = str[i].upcase : str[i] = str[i].downcase
  end
  str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str.split.map {|w| w.length >= 5 ? w.reverse : w}.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  res = []
  for i in (1..n)
    if i % 3 == 0 && i % 5 == 0
      res << "fizzbuzz"
    elsif i % 3 == 0
      res << "fizz"
    elsif i % 5 == 0
      res << "buzz"
    else
      res << i
    end
  end
  res
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  res = []
  until arr.empty?
    res << arr.pop
  end
  res
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  if num == 2
    return true
  elsif num < 2 || num % 2 == 0
    return false
  else
    i = 3
    while i < num / 2
      return false if num % i == 0
      i += 1
    end
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  (1..num).select {|n| num % n == 0}
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  (1..num).select {|n| num % n == 0 && prime?(n)}
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  #Assume there is an oddball
  arr.count(&:odd?) > 1 ? arr.select(&:even?).first : arr.select(&:odd?).first
end
